Source: asio
Section: devel
Priority: optional
Maintainer: Markus Wanner <markus@bluegap.ch>
Homepage: https://think-async.com/
Build-Depends: debhelper (>= 11), libssl-dev,
 libboost-dev, libboost-date-time-dev, libboost-regex-dev,
 libboost-test-dev
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/markus/libasio
Vcs-Git: https://salsa.debian.org/markus/libasio.git

Package: libasio-dev
Section: libdevel
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Recommends: libboost-regex-dev, libssl-dev,
 libboost-dev,
 libboost-date-time-dev
Description: cross-platform C++ library for network programming
 asio is a cross-platform C++ library for network programming that provides
 developers with a consistent asynchronous I/O model using a modern C++
 approach. It has recently been accepted into Boost.
 .
 This package contains the development header files.

Package: libasio-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Recommends: libasio-dev
Description: documentation for asio C++ library for network programming
 asio is a cross-platform C++ library for network programming that provides
 developers with a consistent asynchronous I/O model using a modern C++
 approach. It has recently been accepted into Boost.
 .
 This package contains the documentation and examples.
